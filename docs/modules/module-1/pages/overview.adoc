= Module 1 Overview

This is version *{page-component-version}* of Module 1 in Part B.

== Page source

This page is sourced from [.path]_demo-part-b/docs/modules/module-1/pages/overview.adoc_.

== Cross reference syntax to target this page

To create a cross reference *to this page from another page in Module 1*, the xref syntax would be `\xref:overview.adoc[]`.

To create a cross reference *to this page from a page in the ROOT module of Part B*, the xref syntax would be `\xref:module-1:overview.adoc[]`.

=== Always target the latest version of this page

To create a cross reference *to the latest version of this page from a page in Part A*, the xref syntax would be `\xref:part-b:module-1:overview.adoc[]`.

=== Target a specific version of this page

To create a cross reference *to version 1.0 of this page from a page in Part A*, the xref syntax would be `\xref:1.0@part-b:module-1:overview.adoc[]`.
